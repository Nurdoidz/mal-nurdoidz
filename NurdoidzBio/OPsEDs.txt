[size=175][b]My Favorite OPs/EDs[/b][/size]

[i][url=https://bitbucket.org/Nurdoidz/mal-nurdoidz/history-node/0520c32c0eb5/NurdoidzBio/OPsEDs.txt?at=master]History[/url] - Last updated 2017-01-05[/i]

[size=150][b]OPs[/b][/size]
[list=1]
[*][b][url=http://www.myanimelist.net/anime/205]Samurai Champloo[/url] ([url=https://my.mixtape.moe/gzikeg.webm]OP1[/url])[/b], Score: [b]9.8[/b] (visuals: [b]9.7[/b]; music: [b]9.8[/b]).
[*][b][url=https://myanimelist.net/anime/28999]Charlotte[/url] ([url=https://my.mixtape.moe/tjjydi.webm]OP1[/url])[/b], Score: [b]9.8[/b] (visuals: [b]9.8[/b]; music: [b]9.8[/b]).
[*][b][url=http://www.myanimelist.net/anime/13601]Psycho-Pass[/url] ([url=https://my.mixtape.moe/fzgohg.webm]OP1[/url])[/b], Score: [b]9.7[/b] (visuals: [b]9.6[/b]; music: [b]9.7[/b]).
[*][b][url=https://myanimelist.net/anime/30911]Tales of Zestiria the X[/url] ([url=https://track3.mixtape.moe/ingrnl.webm]OP1[/url])[/b], Score: [b]9.7[/b] (visuals: [b]9.6[/b]; music: [b]9.7[/b]).
[*][b][url=https://myanimelist.net/anime/32729]Orange[/url] ([url=https://track3.mixtape.moe/kmdygu.webm]OP1[/url])[/b], Score: [b]9.7[/b] (visuals: [b]9.5[/b]; music: [b]9.9[/b]).
[*][b][url=https://myanimelist.net/anime/2966]Spice and Wolf[/url] ([url=https://my.mixtape.moe/gosesn.webm]OP1[/url])[/b], Score: [b]9.6[/b] (visuals: [b]9.1[/b]; music: [b]10.0[/b]).
[*][b][url=http://www.myanimelist.net/anime/8525]The World God Only Knows[/url] ([url=https://my.mixtape.moe/djhfcj.webm]OP1[/url])[/b], Score: [b]9.6[/b] (visuals: [b]9.6[/b]; music: [b]9.6[/b]).
[*][b][url=https://myanimelist.net/anime/20787]Black Bullet[/url] ([url=https://track4.mixtape.moe/pnplrm.webm]OP1[/url])[/b], Score: [b]9.5[/b] (visuals: [b]9.4[/b]; music: [b]9.5[/b]).
[*][b][url=http://www.myanimelist.net/anime/18119]Servant x Service[/url] ([url=https://my.mixtape.moe/kvasjv.webm]OP1[/url])[/b], Score: [b]9.5[/b] (visuals: [b]9.4[/b]; music: [b]9.5[/b]).
[*][b][url=https://myanimelist.net/anime/10490]Blood-C[/url] ([url=https://track4.mixtape.moe/gsufhk.webm]OP1[/url])[/b], Score: [b]9.5[/b] (visuals: [b]9.5[/b]; music: [b]9.5[/b]).
[*][b][url=http://www.myanimelist.net/anime/31163]Dimension W[/url] ([url=https://my.mixtape.moe/hpadwz.webm]OP1[/url])[/b], Score: [b]9.3[/b] (visuals: [b]9.6[/b]; music: [b]8.9[/b]).
[*][b][url=http://www.myanimelist.net/anime/9253]Steins;Gate[/url] ([url=https://my.mixtape.moe/paqxxh.webm]OP1[/url])[/b], Score: [b]9.3[/b] (visuals: [b]9.6[/b]; music: [b]9.0[/b]).
[*][b][url=https://myanimelist.net/anime/31798]Kiznaiver[/url] ([url=https://track3.mixtape.moe/chwhuw.webm]OP1[/url])[/b], Score: [b]9.3[/b] (visuals: [b]9.6[/b]; music: [b]9.0[/b]).
[*][b][url=http://www.myanimelist.net/anime/17895]Golden Time[/url] ([url=https://track3.mixtape.moe/gikqig.webm]OP2[/url])[/b], Score: [b]9.2[/b] (visuals: [b]8.9[/b]; music: [b]9.5[/b]).
[*][b][url=https://myanimelist.net/anime/14289]Say "I Love You"[/url] ([url=https://track4.mixtape.moe/mxpczt.webm]OP1[/url])[/b], Score: [b]9.1[/b] (visuals: [b]8.7[/b]; music: [b]9.5[/b]).

[/list]
[size=150][b]EDs[/b][/size]
[list=1]
[*][b][url=http://www.myanimelist.net/anime/13125]From the New World[/url] ([url=https://my.mixtape.moe/tdfgmf.webm]ED1[/url])[/b], Score: [b]9.8[/b] (visuals: [b]9.8[/b]; music: [b]9.7[/b]).
[*][b][url=https://myanimelist.net/anime/32979]Flip Flappers[/url] ([url=https://track4.mixtape.moe/ulyvbo.webm]ED1[/url])[/b], Score: [b]9.6[/b] (visuals: [b]9.5[/b]; music: [b]9.6[/b]).
[*][b][url=http://www.myanimelist.net/anime/205]Samurai Champloo[/url] ([url=https://my.mixtape.moe/crtydu.webm]ED2[/url])[/b], Score: [b]9.3[/b] (visuals: [b]9.0[/b]; music: [b]9.6[/b]).
[*][b][url=http://www.myanimelist.net/anime/205]Samurai Champloo[/url] ([url=https://my.mixtape.moe/dyzjzx.webm]ED1[/url])[/b], Score: [b]9.1[/b] (visuals: [b]9.0[/b]; music: [b]9.2[/b]).
[*][b][url=http://myanimelist.net/anime/1210]Welcome to the NHK![/url] ([url=http://web.archive.org/web/20150611115403/http://a.pomf.se/eqnadt.webm]ED1[/url])[/b], Score: [b]9.1[/b] (visuals: [b]9.0[/b]; music: [b]9.1[/b]).
[*][b][url=http://www.myanimelist.net/anime/9253]Steins;Gate[/url] ([url=https://my.mixtape.moe/mryhkc.webm]ED2[/url] (spoilers))[/b], Score: [b]9.0[/b] (visuals: [b]8.7[/b]; music: [b]9.3[/b]).
[*][b][url=http://www.myanimelist.net/anime/30831]KonoSuba: God's Blessing on This Wonderful World![/url] ([url=https://my.mixtape.moe/ivkeud.webm]ED1[/url])[/b], Score: [b]9.0[/b] (visuals: [b]9.4[/b]; music: [b]8.6[/b]).
[*][b][url=http://www.myanimelist.net/anime/13759]The Pet Girl of Sakurasou[/url] ([url=http://b.1339.cf/pkrjrae.webm]ED3[/url] (spoilers))[/b], Score: [b]8.9[/b] (visuals: [b]8.8[/b]; music: [b]9.0[/b]).
[*][b][url=http://www.myanimelist.net/anime/31163]Dimension W[/url] ([url=http://b.1339.cf/tezrzui.webm]ED1[/url])[/b], Score: [b]8.9[/b] (visuals: [b]8.7[/b]; music: [b]9.0[/b]).
[*][b][url=http://www.myanimelist.net/anime/4214]Rosario to Vampire Capu2[/url] ([url=https://my.mixtape.moe/guyecc.webm]ED1[/url])[/b], Score: [b]8.5[/b] (visuals: [b]8.3[/b]; music: [b]8.6[/b]).
[/list]
